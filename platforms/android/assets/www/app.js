 $('#textarea1').trigger('autoresize');

$.get( "https://qu-be.fr/app/token.php", function( data ) {
  sessionStorage.setItem("token",data);
});

function tout() {

	$('button#btnweb, button#btngraphisme, button#btnhebergement').removeClass('active');
	$('#hebergement,#web,#graphisme').show();
	$('button#btnall').addClass('active');

}

function hebergement() {

	$('button#btnweb, button#btngraphisme, button#btnall').removeClass('active');
	$('#web,#graphisme').hide();
	$('#hebergement').show();
	$('button#btnhebergement').addClass('active');

}

function web() {

	$('button#btnhebergement, button#btngraphisme, button#btnall').removeClass('active');
	$('#hebergement,#graphisme').hide();
	$('#web').show();
	$('button#btnweb').addClass('active');
	
}

function graphisme() {

	$('button#btnhebergement, button#btnweb, button#btnall').removeClass('active');
	$('#hebergement,#web').hide();
	$('#graphisme').show();
	$('button#btngraphisme').addClass('active');
	
}

function mail() {

	$.post( "https://qu-be.fr/app/test.php", { nom: $('#nom').val(), mail: $('#email').val(), sujet: $('#sujet').val(), texte: $('#texte').val(), token: sessionStorage.getItem("token")})
	  .done(function( data ) {
	    	alert('Le message a bien été envoyé, nous vous recontacterons au plus vite.');
	    	$('#nom, #email, #sujet, #texte').val('');
	  });

}