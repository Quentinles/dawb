/// <reference path="WikitudePlugin.d.ts" />
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { TabsPage } from '../pages/tabs/tabs';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = TabsPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();

      /** Enter your Wikitude (trial) License Key here */
      WikitudePlugin._sdkKey = "W4HlLfBbsNdRWdZ3xQCeCwm021D1MPHx84xSdvzGcs/E3RZdJnX6S/smCz4SfJqEI0sFErSzUvVoklCL2t2nT/CvOTIKkDkWzOBzkBXGeANCFPObX8VGtSw/FMbyD7yhFWy+1VxpQ9hOOFqk4LzJ4CCFNCxzFJaQZHnL6cIMXttTYWx0ZWRfX35cRZ3dEjUZYlFP0ZrF37GQKmR19J6ZY30Al+GgFTxb/lCte5BtEPFXndsVwY4wLBHWGUXvKYdG8Pu5Ffp9lAl1OxgmvBT7aPHCPRVlgYkN+BIamZ3fncpTWQpoAo6My0zv3yh+JMDLvPT7aMB/XZU4Y3UiQ1ThDMn8TyVyZTum+KZZkUD4UWaRN5yFrnv1f6YbowsxbqqDmDS7OPsRvbdfDuieUEMWtgYlD9ZtzakstuwtsGIHIp47j9kWi4YlK0aiZ486T3m047IBF2pHd+DDAsrbIoCmYlmVb/Vdip070arZJvwWijNk7qsiSnpfN3FGzYmZeDe53zl5henMmIlYLs373IBhZvRZe/o/MNmj6xuKg5+/E5YIKwClpPr2BC36xci0BmDtfkQ8CUlGrdtc3JOY3MXROaPrrb67epyHf1CMUzyzRVfrz5lncQKEuNg/NKYFysWd4vJs3K9CwaDGdHPPaAVT0NSf8LFpJj7tDaI3ZoT9/9o=";

      /** Check if your device supports AR */
      WikitudePlugin.isDeviceSupported(
          function(success) {
            console.log("Your platform supports AR/Wikitude. Have fun developing!!");
          },
          function(fail) {
            console.log("Your platform failed to run AR/Wikitude: "+fail);
          },
          [WikitudePlugin.FeatureGeo] // or WikitudePlugin.Feature2DTracking 
      );                  

      /** The Wikitude AR View creates it's own context. Communication between the main Ionic App and Wikitude SDK works 
       * through the function below for the direction Ionic2 app --> Wikitude SDK 
       * For calls from Wikitude SDK --> Ionic2 app see the captureScreen example in 
       * WikitudeIonic2StarterApp/www/assets/3_3dModels_6_3dModelAtGeoLocation/js/3dmodelatgeolocation.js*/
      // set the function to be called, when a "communication" is indicated from the AR View  
      WikitudePlugin.setOnUrlInvokeCallback(function(url) {

        // this an example of how to receive a call from a function in the Wikitude SDK (Wikitude SDK --> Ionic2)
        if (url.indexOf('captureScreen') > -1) {
            WikitudePlugin.captureScreen(
                (absoluteFilePath) => {
                    console.log("snapshot stored at:\n" + absoluteFilePath);

                    // this an example of how to call a function in the Wikitude SDK (Ionic2 app --> Wikitude SDK)
                    WikitudePlugin.callJavaScript("World.testFunction('Screenshot saved at: " + absoluteFilePath +"');");
                },
                (errorMessage) => {
                    console.log(errorMessage);
                },
                true, null
            );
        } else {
            alert(url + "not handled");
        }
      });

      /**
       * Define the generic ok callback
       */
      WikitudePlugin.onWikitudeOK = function() {
          console.log("Things went ok.");
      }
      
      /**
       * Define the generic failure callback
       */
      WikitudePlugin.onWikitudeError = function() {
          console.log("Something went wrong");
      }

      // Just as an example: set the location within the Wikitude SDK, if you need this (You can get the geo coordinates by using ionic native 
      // GeoLocation plugin: http://ionicframework.com/docs/v2/native/geolocation/
      //WikitudePlugin.setLocation(47, 13, 450, 1);

      /* for Android only
      WikitudePlugin.setBackButtonCallback(
          () => {
              console.log("Back button has been pressed...");
          }
      );                  
      */

    });
  }
}
